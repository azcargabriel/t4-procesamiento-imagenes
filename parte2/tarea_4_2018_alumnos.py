#####################################################################################################
# testing VGG face model using a pre-trained model
# written by Zhifei Zhang, Aug., 2016
#####################################################################################################

from vgg_face import vgg_face
#from scipy.misc import imread, imresize#, imwrite
from imageio import imread, imwrite
from skimage.transform import resize
import tensorflow as tf
import numpy as np
import os

# getFeatures: bool -> None
# Obtiene las caracteristicas de la ultima capa si se le ingresa un True
# Obtiene las caracteristicas de la penultima capa si se le ingresa un False
def getFeatures(last):
	if last:
		arg = 'fc8'
		n = 2622
	else:
		arg = 'fc7'
		n = 4096
	# build the graph
	graph = tf.Graph()
	with graph.as_default():
	    input_maps = tf.placeholder(tf.float32, [None, 224, 224, 3])
	    output, average_image, class_names = vgg_face('vgg-face.mat', input_maps)
	    feats = output[arg]

	with tf.Session(graph=graph) as sess:
		path = "images/"
		destination_path = "features/"
		folders = ["Asian/", "Black/", "Indian/", "Others/", "White/"]
		i = 0
		first_element = True
		while i < 1000:
			for filename in os.listdir(path + folders[int(i/200)]):
				full_filename = path + folders[int(i/200)] + filename
				#Get features
				img = imread(full_filename, pilmode='RGB')
				img = img[0:250, :, :]
				img = resize(img, [224, 224])  #imresize
				img = img - average_image
				[feat_vals] = sess.run([feats], feed_dict={input_maps: [img]})
				feat_vals = np.reshape(feat_vals, (1, n))
				feat_vals = np.append(feat_vals, int(i/200))
				if first_element:
					final = feat_vals
					first_element = False
				else:
					final = np.vstack((final, feat_vals))
				print("Done " + full_filename)
				i += 1
		#Save
		np.savetxt(arg+"_db.csv", final, delimiter=",")

getFeatures(last=True)
getFeatures(last=False)