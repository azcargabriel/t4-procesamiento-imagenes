import pandas as pd
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score

from sklearn.preprocessing import StandardScaler

def show_confusion_matrix(m, title):
    fig, ax = plt.subplots()    
    ax.matshow(m, cmap=plt.cm.Wistia)
    
    for i in range(5):
        for j in range(5):
            c = m[j,i]
            ax.text(i, j, str(c), va='center', ha='center')
            
    plt.savefig(title+'_confusion_matrix.png', format='png', dpi=1000)

def classify(last):
    if last:
	    filename = 'fc8_db.csv'
	    n = 2622
	    layer = 'last'
    else:
        filename = 'fc7_db.csv'
        n = 4096
        layer = 'last-1'
    #Entrenamiento
    f = pd.read_csv(filename, header=None)
    X = StandardScaler().fit_transform(f.values[:, 0:n])
    y = f.values[:, n]
    trainX, testX, trainY, testY = train_test_split(X, y, test_size=0.3)
    
    clf = svm.SVC(kernel='linear')
    clf.fit(trainX, trainY)
    
    #Prueba
    scores = clf.predict(testX)
    acc = accuracy_score(testY, scores)
    cm = confusion_matrix(testY, scores)
    
    show_confusion_matrix(cm, layer)
    print(layer + " Acc: " + str(acc))
    
classify(last=True)
classify(last=False)